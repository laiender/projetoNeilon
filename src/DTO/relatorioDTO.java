/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;
import persistencia.Conexao;

/**
 *
 * @author laiendercamargos
 */
public class relatorioDTO {
    
    public static void ralatoiroJogador(){
        try {
            Conexao conect = new Conexao();
            Statement st = conect.getSt();
            st.execute("SELECT * FROM pessoa");
            JRResultSetDataSource relResult = new JRResultSetDataSource(conect.getSt().getResultSet());
            JasperPrint jpPrint = JasperFillManager.fillReport("Relatorios/RelatorioJogadores.jasper", new HashMap(), relResult);
            JasperViewer jv = new JasperViewer(jpPrint, false);
            jv.setVisible(true);
            jv.toFront();
        } catch (SQLException e) {
              JOptionPane.showMessageDialog(null, "erro SQL ao exibir Relatorio de jogador. "+ e);
        } catch(Exception erro){
            JOptionPane.showMessageDialog(null,"erro ao exibir Relatorio de jogador. "+ erro);
        }

            
        
    }
    
}
