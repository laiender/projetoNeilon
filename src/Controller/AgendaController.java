/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.AgendaDAO;
import controle.Agenda;

/**
 *
 * @author laiendercamargos
 */
public class AgendaController {
    
    public static boolean persistir(Agenda agenda) throws Exception{
        //JOptionPane.showMessageDialog(null, "Comtroler");
        return AgendaDAO.persistir(agenda);
    }
    
    public static void atualizar(Agenda agenda){
        AgendaDAO.atualizar(agenda);
    }

    public static void deletar(int idAgenda) {
        AgendaDAO.delete (idAgenda);
    }
    
}
