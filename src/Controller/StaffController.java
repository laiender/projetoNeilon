/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.JogadorDAO;
import DAO.StaffDAO;
import controle.Jogador;
import controle.Pessoa;

/**
 *
 * @author laiendercamargos
 */
public class StaffController {
    public static boolean persistir(Pessoa staff) throws Exception{
        return StaffDAO.persistir(staff);
    }
    public static Pessoa pesquisa (Pessoa nome) throws Exception{
        return StaffDAO.pesquisa(nome);
    }

    public static void atualizar(Pessoa staffAtualizar) {
        StaffDAO.atualizar(staffAtualizar);
    }

    public static void deletar(int idPessoa) {
        StaffDAO.delete(idPessoa);
    }
}
