/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.ContratoDAO;
import controle.Contrato;
import controle.Jogador;

/**
 *
 * @author laiendercamargos
 */
public class CadastroContratoController {

    public static boolean persistir(Jogador contrato) {
     return ContratoDAO.pesistir(contrato);
    }
    
    public static Jogador pesquisa(Jogador contrato, int idJogador){
        return ContratoDAO.pesquisa(contrato, idJogador);
    }
    
    public static void atualizar(Jogador contrato){
        ContratoDAO.atualizar(contrato);
    }

    //public static boolean persistir(Contrato contrato) {
    //    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    //}
    
}
