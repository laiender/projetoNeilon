/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.PatrocinadorDAO;
import controle.Patrocinador;

/**
 *
 * @author laiendercamargos
 */
public class CadastroPatrocinadorController {

    public static boolean persistir(Patrocinador patrocinador) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return PatrocinadorDAO.persistir(patrocinador);
    }

    public static Patrocinador pesquisa(Patrocinador patrocinador) throws Exception {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return PatrocinadorDAO.pesquisaCNPJ(patrocinador);
    }

    public static void deletar(int idPatrocinado) {
        PatrocinadorDAO.delete(idPatrocinado);
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static void atualizar(Patrocinador patrocinador) {
        PatrocinadorDAO.atualizar(patrocinador);
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    
}
