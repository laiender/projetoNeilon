/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.ContaBancariaDAO;
import controle.Conta;
import controle.Jogador;
import java.sql.SQLException;
import javax.swing.JTextField;

/**
 *
 * @author laiendercamargos
 */
public class ContaBancariaController {

    public static void presistir(Jogador contaJogador) throws SQLException {
        ContaBancariaDAO.persisitir(contaJogador);
    }
    public static Jogador pesquisa (Jogador jogador ,int numeroConta) throws Exception{
        return ContaBancariaDAO.pesquisa(jogador, numeroConta);
    }

    public static void atualizar(Jogador jogador) {
        ContaBancariaDAO.atualizar(jogador);
    }

 
    
}
