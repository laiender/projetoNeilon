/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import controle.Patrocinador;
import controle.Renda;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import persistencia.Conexao;

/**
 *
 * @author laiendercamargos
 */
public class CadastroRendaDAO {

    public static boolean persistir(Renda renda) {
        try {
            Conexao conect = new Conexao();
            Statement st = conect.getSt();
            java.sql.Date dataRenda = new java.sql.Date(renda.getData().getTime());
            st.executeUpdate("insert into cadastroRenda (origemRenda, valor, data, patrocinador) values('"
                    + renda.getOrigem() + "','"
                    + renda.getValor() + "','"
                    + dataRenda + "','"
                    + renda.getPatrocinador().getRazaoSocial()
                    +"')");
            
        } catch (SQLException e) {
            System.err.println("Erro SQL  " + e );
        }catch (Exception u){
            System.err.println("Erro ao cadastra renda " + u );
        }
        return false;
    }

    public ArrayList<Renda> listarRenda() throws SQLException{
        ArrayList<Renda> rendaLista = new ArrayList<Renda>();
        try {
            Conexao conect = new Conexao();
            PreparedStatement st = conect.getConnection().prepareStatement("SELECT valor, origemRenda, data, patrocinador FROM cadastroRenda");
            ResultSet rs = st.executeQuery();
            while(rs.next()){
                Renda renda = new Renda();
                
                renda.setValor(rs.getDouble("valor"));
                renda.setOrigem(rs.getString("origemRenda"));
                renda.setData(rs.getDate("data"));
                //renda.setPatrocinador(patrocinador);
                rendaLista.add(renda);
            }
            conect.fecharConexao();
        } catch (SQLException erro) {
            System.err.println("erro SQL " + erro );
        }catch (Exception e){
            System.err.println("erro ao listar renda "  + e );
        }
        
        return rendaLista;
    }
     
    
}
