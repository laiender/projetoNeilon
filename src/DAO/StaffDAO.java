/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import com.jaspersoft.jasperserver.ws.scheduling.ResultSendType;
import controle.Contato;
import controle.Contrato;
import controle.Endereco;
import controle.Jogador;
import controle.Pessoa;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import org.olap4j.PreparedOlapStatement;
import persistencia.Conexao;

/**
 *
 * @author laiendercamargos
 */
public class StaffDAO {
    
    public static boolean persistir(Pessoa staff) throws Exception{
        try {
            Conexao conect = new Conexao();
            Statement st = conect.getSt();
            java.sql.Date data = new java.sql.Date(staff.getDataNascimento().getTime());
            st.executeUpdate("insert into pessoa(nome, sobrenome, nacionalidade, rg, cpf, "
                    + "dataNascimento, escolaridade, instituicao, sexo )"
                    + " values ('" +staff.getNome()+ "','"
                    + staff.getSobreNome() + "','"
                    + staff.getNacionalidade() + "','" 
                    + staff.getRg() + "','" 
                    + staff.getCpf() + "','"
                    + data + "','"
                    + staff.getEscolaridade() + "','"
                    + staff.getInstituicao() + "','"
                    + staff.getSexo()
                    + "')", Statement.RETURN_GENERATED_KEYS);
            final ResultSet rs = st.getGeneratedKeys();
            
            if(rs.next()){
                staff.setIdPessoa(rs.getInt(1));
            }
             st.executeUpdate("insert into endereco (rua, numero, bairro, cidade, "
                    + "estado, pais, complemento, cep, idPessoa_fk)"
                    + " values('" + staff.getEndereco().getRua() + "','"
                    + staff.getEndereco().getNumero() + "','"
                    + staff.getEndereco().getBairro() + "','"
                    + staff.getEndereco().getCidade() + "','"
                    + staff.getEndereco().getEstado() + "','"
                    + staff.getEndereco().getPais() + "','"
                    + staff.getEndereco().getComplemento() + "','"
                    + staff.getEndereco().getCep() + "','"
                    + staff.getIdPessoa()
                    + "')");
             
             st.executeUpdate("insert into contatos (telefoneResidencial, celular, email, idPessoa_fk)"
                    + "values('" + staff.getContato().getTelefoneResidencial() + "','"
                    + staff.getContato().getCelular() + "','"
                    + staff.getContato().getEmail() + "','"
                    + staff.getIdPessoa()
                    + "')");
             st.executeUpdate("insert into staff (funcao, idPessoa_fk) "
                     + "values ('"+staff.getFuncao() + "','"
                     + staff.getIdPessoa()
                     + "')");
             java.sql.Date dataInicio = new java.sql.Date(staff.getContrato().getInicio().getTime());
             st.executeUpdate("insert into contrato (dataInicio, clausulas, salario, idPessoa_fk)"
                     + " values ('"+ dataInicio +"','"
                     + staff.getContrato().getClausulas() + "','"
                     + staff.getContrato().getSalario() + "','"
                     + staff.getIdPessoa()
                     + "')");
             
             conect.fecharConexao();
            
        } catch (SQLException e) {
            System.err.println("Erro SQL ao salvar o Staff" + e);
            e.printStackTrace();
        }catch(Exception erro){
            System.err.println("Erro ao salvar o Staff " + erro);
        }
        
        return false;
    }

    public static Pessoa pesquisa(Pessoa cpf) {
        Pessoa staff = null;
        String cpfStaff =  cpf.getCpf();
        //JOptionPane.showMessageDialog(null,"nome na pesquisa   "+ cpfStaff);
        try {
            Conexao conect = new Conexao();
            PreparedStatement st = conect.getConnection().prepareStatement("SELECT pessoa.idPessoa FROM pessoa where pessoa.cpf = ?");
            st.setString(1, cpfStaff);
            ResultSet rs = st.executeQuery();
            
            if(rs.next()){
                staff = new Pessoa();
                staff.setIdPessoa(rs.getInt("idPessoa"));
                JOptionPane.showMessageDialog(null, "idPessoa pesquisada  "+staff.getIdPessoa());
                pesquisaID(staff);
            }
            //JOptionPane.showMessageDialog(null,"id "+ nomeStaff);
                
            conect.fecharConexao();
        } catch (SQLException erro) {
            System.err.println("Erro SQL ao pesquisar o ID pessoa. " + erro);
        } catch(Exception e){
            System.err.println("Erro na pesquisa " + e);
        }
        
        return staff;
    }
    
    public static Pessoa pesquisaID(Pessoa staffPesquisa) throws Exception{
        int idPessoa = staffPesquisa.getIdPessoa();
        
        JOptionPane.showMessageDialog(null," id Staff na pesquisa "+ idPessoa);
        
        try {
            Conexao conect = new Conexao();
            PreparedStatement st = conect.getConnection().prepareStatement("SELECT pessoa.nome, pessoa.sobrenome, "
                    + " pessoa.nacionalidade, pessoa.rg, pessoa.cpf, pessoa.dataNascimento, pessoa.escolaridade, "
                    + "pessoa.instituicao, pessoa.sexo , staff.funcao, "
                    + "contatos.telefoneResidencial, contatos.celular, contatos.email, endereco.rua, "
                    + "endereco.numero, endereco.bairro, endereco.cidade, endereco.estado, endereco.pais, "
                    + "endereco.complemento, endereco.cep, contrato.dataInicio, contrato.clausulas, contrato.salario "
                    + "FROM pessoa, staff, contatos, endereco, contrato "
                    + "WHERE idPessoa = ? AND idPessoa = staff.idPessoa_fk AND " 
                    + "idPessoa = contatos.idPessoa_fk AND idPessoa = endereco.idPessoa_fk "
                    + "AND idPessoa = contrato.idPessoa_fk");
            st.setInt(1, idPessoa);
            ResultSet rs = st.executeQuery();
            if(rs.next()){
                staffPesquisa.setNome(rs.getString("pessoa.nome"));
                staffPesquisa.setSobreNome(rs.getString("pessoa.sobrenome"));
                staffPesquisa.setNacionalidade(rs.getString("pessoa.nacionalidade"));
                staffPesquisa.setRg(rs.getString("pessoa.rg"));
                staffPesquisa.setCpf(rs.getString("pessoa.cpf"));
                staffPesquisa.setDataNascimento(rs.getDate("pessoa.dataNascimento"));
                staffPesquisa.setEscolaridade(rs.getString("pessoa.escolaridade"));
                staffPesquisa.setInstituicao(rs.getString("pessoa.instituicao"));
                staffPesquisa.setSexo(rs.getString("pessoa.sexo"));
                staffPesquisa.setFuncao(rs.getString("staff.funcao"));
                staffPesquisa.setContato(new Contato (rs.getString("contatos.telefoneResidencial"), rs.getString("contatos.celular"), 
                    rs.getString("contatos.email")));
                //JOptionPane.showMessageDialog(null, "telefone residencial apos a pesquisa   "+ jogadorPesquisa.contato.getTelefoneResidencial());
                staffPesquisa.setEndereco(new Endereco((rs.getString("endereco.rua")),
                        (rs.getString("endereco.numero")),
                        (rs.getString("endereco.bairro")),
                        (rs.getString("endereco.cidade")),
                        (rs.getString("endereco.estado")),
                        (rs.getString("endereco.pais")),
                        (rs.getString("endereco.complemento")),
                        (rs.getString("endereco.cep"))));
                staffPesquisa.setContrato(new Contrato(rs.getDate("contrato.dataInicio"), rs.getString("contrato.clausulas"), 
                    rs.getDouble("contrato.salario")));
                
                conect.fecharConexao();
                
                
            }
        }catch(SQLException erro ){
            System.err.println("Erro SQL na pesquisa:   " + erro);
        }catch (Exception e) {
            System.err.println("Erro na pesquisa:    na clase DAO"+ e);
        }
        
        //JOptionPane.showMessageDialog(null,staffPesquisa.toString());
        return staffPesquisa;
    }

    public static void atualizar(Pessoa staffAtualizar) {
        if(staffAtualizar == null){
            JOptionPane.showMessageDialog(null,"STAFF não encontrado..");
        }
        try {
            Conexao conect = new Conexao();
            String sql;
            java.sql.Date data = new java.sql.Date(staffAtualizar.getDataNascimento().getTime());
            java.sql.Date dataInicio = new java.sql.Date(staffAtualizar.getContrato().getInicio().getTime());
            
            sql = "UPDATE pessoa INNER JOIN contatos ON pessoa.idPessoa = contatos.idPessoa_fk "
                    + "INNER JOIN endereco ON pessoa.idPessoa = endereco.idPessoa_fk "
                    + "INNER JOIN staff ON pessoa.idPessoa = staff.idPessoa_fk  "
                    + "INNER JOIN contrato ON pessoa.idPessoa = contrato.idPessoa_fk "
                    + "SET pessoa.nome =?, pessoa.sobrenome =?, "
                    + "pessoa.nacionalidade =?, pessoa.rg =?, pessoa.cpf =?, pessoa.dataNascimento =?, pessoa.escolaridade =?, "
                    + "pessoa.instituicao =?, pessoa.sexo =?, staff.funcao =?, "
                    + "contatos.telefoneResidencial =?, contatos.celular =?, contatos.email =?, endereco.rua =?, "
                    + "endereco.numero =?, endereco.bairro =?, endereco.cidade =?, endereco.estado =?, endereco.pais =?, "
                    + "endereco.complemento =?, endereco.cep =?, contrato.dataInicio =?, contrato.clausulas =?, contrato.salario =? "
                    + " WHERE pessoa.idPessoa =? ";
            
            PreparedStatement st = conect.getConnection().prepareStatement(sql);
            st.setString(1, staffAtualizar.getNome());
            st.setString(2, staffAtualizar.getSobreNome());
            st.setString(3, staffAtualizar.getNacionalidade());
            st.setString(4, staffAtualizar.getRg());
            st.setString(5, staffAtualizar.getCpf());
            st.setDate(6, data);
            st.setString(7, staffAtualizar.getEscolaridade());
            st.setString(8, staffAtualizar.getInstituicao());
            st.setString(9, staffAtualizar.getSexo());
            st.setString(10, staffAtualizar.getFuncao());
            st.setString(11, staffAtualizar.getContato().getTelefoneResidencial());
            st.setString(12, staffAtualizar.getContato().getCelular());
            st.setString(13, staffAtualizar.getContato().getEmail());
            st.setString(14, staffAtualizar.getEndereco().getRua());
            st.setString(15, staffAtualizar.getEndereco().getNumero());
            st.setString(16, staffAtualizar.getEndereco().getBairro());
            st.setString(17, staffAtualizar.getEndereco().getCidade());
            st.setString(18, staffAtualizar.getEndereco().getEstado());
            st.setString(19, staffAtualizar.getEndereco().getPais());
            st.setString(20, staffAtualizar.getEndereco().getComplemento());
            st.setString(21, staffAtualizar.getEndereco().getCep());
            st.setDate(22, dataInicio);
            st.setString(23, staffAtualizar.getContrato().getClausulas());
            st.setDouble(24, staffAtualizar.getContrato().getSalario());
            st.setInt(25, staffAtualizar.getIdPessoa());
            st.executeUpdate();
            
            JOptionPane.showMessageDialog(null, "Cadastro do Staff atualizado...");
            conect.fecharConexao();
        } catch (SQLException erro) {
            System.err.println("Erro SQL na atualização do Staff: " + erro);
        }catch(Exception e){
            System.err.println("Erro a atualizar o Staff: " + e);
        }
    }

    public static void delete(int idPessoa) {
        
        try {
            Conexao conect = new Conexao();
            
            String sql = "DELETE FROM pessoa WHERE idPessoa = ?";
            PreparedStatement st = conect.getConnection().prepareStatement(sql);
            st.setInt(1, idPessoa);
            st.executeUpdate();
        } catch (Exception e) {
            System.err.println("erro no deletar" + e);
            JOptionPane.showMessageDialog(null, "erro ao deletar " + e);
        }
            JOptionPane.showMessageDialog(null, "Jogador apagador.....");

    }
    
    public ArrayList< Jogador> listar(){
        ArrayList<Jogador> staffLista = new ArrayList<Jogador>();
        try {
            Conexao conect = new Conexao();
            PreparedStatement st = conect.getConnection().prepareStatement("SELECT * "
                    + "FROM pessoa INNER JOIN contatos ON pessoa.idPessoa = contatos.idPessoa_fk "
                    + "INNER JOIN endereco ON pessoa.idPessoa = endereco.idPessoa_fk "
                    + "INNER JOIN staff ON pessoa.idPessoa = staff.idPessoa_fk");
            ResultSet rs = st.executeQuery();
            
            while(rs.next()){
                Jogador pessoa = new Jogador();
                pessoa.setCpf(rs.getString("pessoa.cpf"));
                pessoa.setNome(rs.getString("pessoa.nome"));
                pessoa.setSobreNome(rs.getString("pessoa.sobrenome"));
                pessoa.setDataNascimento(rs.getDate("pessoa.dataNascimento"));
                pessoa.setFuncao(rs.getString("staff.funcao"));
                
                staffLista.add(pessoa);
            }
        } catch (SQLException erro) {
            System.err.println("ERRO SQL no lista Staff" + erro);
        }catch( Exception e){
            System.err.println("Erro ao lista Staff" + e );
        }
        return staffLista;
    }
    
    
}
