/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import controle.Contrato;
import controle.Jogador;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.sound.midi.SysexMessage;
import javax.swing.JOptionPane;
import persistencia.Conexao;

/**
 *
 * @author laiendercamargos
 */
public class ContratoDAO {

    public static boolean pesistir(Jogador contrato) {
        try {
            Conexao conect = new Conexao();
            Statement st = conect.getSt();
            java.sql.Date dataInicio = new java.sql.Date(contrato.getContrato().getInicio().getTime());
            java.sql.Date dataFim = new java.sql.Date(contrato.getContrato().getFim().getTime());
            System.out.println("comando sql insert   ");
            st.executeUpdate("insert into contrato (dataInicio, dataFim, clubeOrigem, empresario, telefoneEmpresario,"
                    + "clausulas,salario, idJogador_fk) values ('"
                    + dataInicio + "','"
                    + dataFim + "','"
                    + contrato.getContrato().getClubeOrigem() + "','"
                    + contrato.getContrato().getEmpresario() + "','"
                    + contrato.getContrato().getTelefoneEmpresario() + "','"
                    + contrato.getContrato().getClausulas() + "','"
                    + contrato.getContrato().getSalario() + "','"
                    + contrato.getIdJogador()
            + "')");
        } catch (SQLException erroSql) {
            System.err.println("Erro SQL,  " + erroSql);
        }catch(Exception erro){
            System.err.println("erro ao salvar contrato,   "+ erro);
        }
        return false;
    }
    public static Jogador pesquisa(Jogador contrato , int idJogador){
        //int idJogador = contrato.getIdJogador();
        try {
            Conexao conect = new Conexao();
            PreparedStatement st = conect.getConnection().prepareStatement("SELECT contrato.dataInicio, contrato.dataFim,"
                    + "contrato.clubeOrigem, contrato.empresario, contrato.telefoneEmpresario, "
                    + "contrato.clausulas, contrato.salario "
                    + "FROM contrato WHERE idJogador_fk = ? ");
            st.setInt(1, idJogador);
            ResultSet rs = st.executeQuery();
            if(rs.next()){
                contrato.setContrato(new Contrato (
                        rs.getDate("contrato.dataInicio"), 
                        rs.getDate("contrato.dataFim"), 
                        rs.getString("contrato.clubeOrigem"), 
                        rs.getString("contrato.empresario"), 
                        rs.getString("contrato.telefoneEmpresario"), 
                        rs.getString("contrato.clausulas"),
                        rs.getDouble("contrato.salario")));
                
                conect.fecharConexao();
            }
        } catch (SQLException erro) {
            System.err.println("Erro SQL na pesquisa de contrato do jogador. " + erro);
            erro.printStackTrace();
        }catch(Exception e ){
            System.err.println("Erro na pesquisa de contrato do jogador.  " +e);
            e.printStackTrace();
        }
            
        return contrato;
    }
    
    public static void atualizar( Jogador jogador){
        if(jogador == null){
            JOptionPane.showMessageDialog(null, "O contrato nao encontrado ");
        }
        
        try {
            Conexao conect = new Conexao();
            String sql;
            java.sql.Date dataInicio = new java.sql.Date(jogador.getContrato().getInicio().getTime());
            java.sql.Date dataFim = new java.sql.Date(jogador.getContrato().getFim().getTime());
            
            sql = "UPDATE contrato SET contrato.dataInicio =? , contrato.dataFim =? ,"
                    + "contrato.clubeOrigem =? , contrato.empresario =? , "
                    + "contrato.telefoneEmpresario =? , contrato.clausulas =?, "
                    + "contrato.salario =? ";
            PreparedStatement st = conect.getConnection().prepareStatement(sql);
            st.setDate(1, dataInicio);
            st.setDate(2, dataFim);
            st.setString(3, jogador.getContrato().getClubeOrigem());
            st.setString(4, jogador.getContrato().getEmpresario());
            st.setString(5, jogador.getContrato().getTelefoneEmpresario());
            st.setString(6, jogador.getContrato().getClausulas());
            st.setDouble(7, jogador.getContrato().getSalario());
            st.executeUpdate();
            
            JOptionPane.showMessageDialog(null, "Contrato Atualizado com Sucesso ..");
            conect.fecharConexao();
            
                    
        } catch (SQLException erro) {
            System.err.println("Erro SQL ao Atualizar Contrato...." + erro);
            erro.printStackTrace();
        }catch(Exception e){
            System.err.println("Erro ao Atualizar o contrato... " + e);
            e.printStackTrace();
        }
    }
}
