/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import controle.Conta;
import controle.Jogador;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import persistencia.Conexao;

/**
 *
 * @author laiendercamargos
 */
public class ContaBancariaDAO {

    public static void persisitir(Jogador contaJogador) throws SQLException {
        Conexao conect = new Conexao();
        Statement st = conect.getSt();
        st.executeUpdate("insert into conta(banco, numeroConta, "
                + "agencia, titularConta, codigoOrdenacao, "
                + "tipoConta, idPessoa_fk) values ('"
                + contaJogador.getConta().getBanco() +"','"
                + contaJogador.getConta().getNumeroConta() +"','"
                + contaJogador.getConta().getNumeroAgencia() + "','"
                + contaJogador.getConta().getTitularConta() + "','"
                + contaJogador.getConta().getCodigoOrdenacao() + "','"
                + contaJogador.getConta().getTipoConta()  + "','"
                + contaJogador.getIdPessoa()
                + "')");
    }
 
    public static Jogador pesquisa(Jogador jogador, int idPessoa) throws Exception {
                
        try {
            Conexao conect = new Conexao();
            PreparedStatement st = conect.getConnection().prepareStatement("SELECT * FROM conta where idPessoa_fk = ?");
            st.setInt(1, idPessoa);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                jogador = new Jogador();
                jogador.setIdPessoa(rs.getInt("conta.idPessoa_fk"));
                jogador.setConta(new Conta(rs.getString("conta.banco"), 
                        rs.getString("conta.numeroConta"),
                        rs.getString("conta.Agencia"),
                        rs.getString("conta.titularConta"),
                        rs.getString("conta.tipoConta"),
                        rs.getString("conta.codigoOrdenacao")));
                //JOptionPane.showMessageDialog(null, "idPessoa primenra pesquisa  "+ jogador.getIdPessoa());
                //pesquisaID(jogador);
                //return jogador;
            }
            conect.fecharConexao();
        } catch (Exception e) {
            System.out.println("Problemas Ocorreram");
            e.printStackTrace();
            throw new Exception("Erro na pesquisa");

        }

        return jogador;

    }

    public static void atualizar(Jogador jogador) {
        if(jogador == null){
            JOptionPane.showMessageDialog(null, "Conta bancaria na encontrada ");
        }
        
        try {
            Conexao conect = new Conexao();
            String sql;
            sql = "UPDATE conta SET conta.banco =?, conta.numeroConta =?, "
                    + "conta.agencia =?, conta.titularConta =?, conta.codigoOrdenacao =?, "
                    + "conta.tipoConta =? WHERE conta.idPessoa_fk =?";
            
            PreparedStatement st = conect.getConnection().prepareStatement(sql);
            st.setString(1, jogador.getConta().getBanco());
            st.setString(2, jogador.getConta().getNumeroConta());
            st.setString(3, jogador.getConta().getNumeroAgencia());
            st.setString(4, jogador.getConta().getTitularConta());
            st.setString(5, jogador.getConta().getCodigoOrdenacao());
            st.setString(6, jogador.getConta().getTipoConta());
            st.setInt(7, jogador.getIdPessoa());
            st.executeUpdate();
            
            JOptionPane.showMessageDialog(null, "dasdos bancarios cadastrados com sucesso..");
            conect.fecharConexao();
        } catch (SQLException erro) {
            System.err.println("Erro SQL ao atualizar os dados bancarios. " + erro);
            erro.printStackTrace();
        }catch(Exception e){
            System.err.println("Erro ao atualizar os dados bancarios. "+ e);
            e.printStackTrace();
        }
    }

  
    
}
