/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import controle.Contato;
import controle.Endereco;
import controle.Patrocinador;
import java.awt.HeadlessException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import persistencia.Conexao;

/**
 *
 * @author laiendercamargos
 */
public class PatrocinadorDAO {

    public static boolean persistir(Patrocinador patrocinador) {
        try {
            Conexao conect = new Conexao();
            Statement st = conect.getSt();

            st.execute("insert into patrocinador (razaoSocial, cnpj, inscricaoEstadual, "
                    + "inscricaoMunicipal, finalidadeLucrativa, nomeFantasia)"
                    + " values('" + patrocinador.getRazaoSocial() + "','"
                    + patrocinador.getCnpj() + "','"
                    + patrocinador.getInscricaoEstadual() + "','"
                    + patrocinador.getInscricaoMunicipal() + "','"
                    + patrocinador.getFinalidadeLucrativa() +"','"
                    + patrocinador.getNomeFantasia()
                    + "')", Statement.RETURN_GENERATED_KEYS);
            final ResultSet rs = st.getGeneratedKeys();
            if (rs.next()) {
                patrocinador.setIdPatrocinador(rs.getInt(1));
            }
            st.executeUpdate("INSERT INTO endereco (rua, numero, bairro, cidade, estado, pais, "
                    + "complemento, cep, idPatrocinador_fk) values ('" + patrocinador.getEndereco().getRua() + "','"
                    + patrocinador.getEndereco().getNumero() + "','"
                    + patrocinador.getEndereco().getBairro() + "','"
                    + patrocinador.getEndereco().getCidade() + "','"
                    + patrocinador.getEndereco().getEstado() + "','"
                    + patrocinador.getEndereco().getPais() + "','"
                    + patrocinador.getEndereco().getComplemento() + "','"
                    + patrocinador.getEndereco().getCep() + "','"
                    + patrocinador.getIdPatrocinador()
                    + "')");

            st.executeUpdate("INSERT INTO contatos (telefoneComercial, celular, email, webSite, idPatrocinador_fk) "
                    + "values ('" + patrocinador.getContato().getTelefoneComercial() + "','"
                    + patrocinador.getContato().getCelular() + "','"
                    + patrocinador.getContato().getEmail() + "','"
                    + patrocinador.getContato().getWebSite() + "','"
                    + patrocinador.getIdPatrocinador()
                    + "')");

            conect.fecharConexao();
        } catch (SQLException e) {
            System.err.println("Erro SQL ao salvar o patrocinador " + e);
            e.printStackTrace();
        }catch (Exception erro){
            System.err.println("Erro ao salvar Patrocinador " + erro);
        }
        return false;
    }

    public static Patrocinador pesquisaCNPJ(Patrocinador patrocinador) throws Exception {
        //Patrocinador patrocinador = null;
        String cnpj = patrocinador.getCnpj();
        JOptionPane.showMessageDialog(null, "getCnpj para pesquisa.  " + patrocinador.getCnpj());
        try {
            Conexao conect = new Conexao();
            PreparedStatement st = conect.getConnection().prepareStatement("SELECT * FROM patrocinador WHERE cnpj = ?");
            st.setString(1, cnpj);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                JOptionPane.showMessageDialog(null, "rs.next  ");
                patrocinador = new Patrocinador();
                patrocinador.setIdPatrocinador(rs.getInt("idPatrocinador"));
                JOptionPane.showMessageDialog(null, "getIdPatrocinador " + patrocinador.getIdPatrocinador());
                pesquisa(patrocinador);
            }

            JOptionPane.showMessageDialog(null, "getIdPatrocinador 2  " + patrocinador.getIdPatrocinador());
            conect.fecharConexao();
        }catch (SQLException e) {
            System.err.println("Erro SQL ao salvar o patrocinador " + e);
        }catch (Exception erro) {
            System.err.println("ERRO ao pesquisar o CNPJ.." + erro);
        }
        return patrocinador;

    }
    
    public static Patrocinador pesquisa(Patrocinador patrocinador) {
        int idPatrocinador = patrocinador.getIdPatrocinador();

        try {
            Conexao conect = new Conexao();
            PreparedStatement st = conect.getConnection().prepareStatement("SELECT patrocinador.razaoSocial, "
                    + "patrocinador.cnpj, patrocinador.inscricaoEstadual, patrocinador.inscricaoMunicipal, "
                    + "patrocinador.finalidadeLucrativa, patrocinador.nomeFantasia , endereco.rua, endereco.numero,"
                    + " endereco.bairro, endereco.cidade, "
                    + "endereco.estado, endereco.pais, endereco.complemento, endereco.cep, "
                    + "contatos.telefoneComercial, contatos.celular, contatos.email, contatos.webSite "
                    + "FROM patrocinador, contatos, endereco "
                    + " WHERE idPatrocinador = ? AND idPatrocinador = contatos.idPatrocinador_fk "
                    + " AND idPatrocinador = endereco.idPatrocinador_fk ");
            st.setInt(1, idPatrocinador);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                patrocinador.setRazaoSocial(rs.getString("patrocinador.razaoSocial"));
                patrocinador.setCnpj(rs.getString("patrocinador.cnpj"));
                patrocinador.setInscricaoEstadual(rs.getString("patrocinador.inscricaoEstadual"));
                patrocinador.setInscricaoMunicipal(rs.getString("patrocinador.inscricaoMunicipal"));
                patrocinador.setFinalidadeLucrativa(rs.getString("patrocinador.finalidadeLucrativa"));
                patrocinador.setNomeFantasia(rs.getString("patrocinador.nomeFantasia"));
                patrocinador.setEndereco(new Endereco((rs.getString("endereco.rua")),
                        (rs.getString("endereco.numero")),
                        (rs.getString("endereco.bairro")),
                        (rs.getString("endereco.cidade")),
                        (rs.getString("endereco.estado")),
                        (rs.getString("endereco.pais")),
                        (rs.getString("endereco.complemento")),
                        (rs.getString("endereco.cep"))));
                patrocinador.setContato(new Contato(rs.getString("contatos.telefoneComercial"), rs.getString("contatos.celular"),
                        rs.getString("contatos.email"), rs.getString("contatos.webSite")));
                conect.fecharConexao();

            }
        }catch (SQLException e) {
            System.err.println("Erro SQL ao salvar o patrocinador " + e);
        }catch (Exception erro) {
            System.err.println("Problema ao pesquisar patrocinador.." + erro);
        }

        return patrocinador;

    }

    public static void delete(int idPatrocinado) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        try {
            Conexao conect = new Conexao();
            String sql = "DELETE FROM patrocinador WHERE idPatrocinador = ? ";
            PreparedStatement st = conect.getConnection().prepareStatement(sql);
            st.setInt(1, idPatrocinado);
            st.executeUpdate();
            conect.fecharConexao();
        } catch (Exception e) {
            System.err.println("ERRO ao Deletar " + e);
            JOptionPane.showMessageDialog(null, "erro ao Deletar patrocinador. " + e);
        }
        JOptionPane.showMessageDialog(null, "Patrociandor Deletado com Sucesso.... ");
    }

    public ArrayList<Patrocinador> listar() throws SQLException {

        ArrayList<Patrocinador> patrocinadorListar = new ArrayList<Patrocinador>();
        try {
            Conexao conect = new Conexao();
            PreparedStatement st = conect.getConnection().prepareStatement("SELECT * "
                    + "FROM patrocinador INNER JOIN contatos ON patrocinador.idPatrocinador =  contatos.idPatrocinador_fk");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Patrocinador patrocinador = new Patrocinador();
                patrocinador.setIdPatrocinador(rs.getInt("patrocinador.idPatrocinador"));
                patrocinador.setRazaoSocial(rs.getString("patrocinador.razaoSocial"));
                patrocinador.setNomeFantasia(rs.getString("patrocinador.nomeFantasia"));
                patrocinador.setCnpj(rs.getString("patrocinador.cnpj"));
                patrocinador.setContato(new Contato( (rs.getString("contatos.email")),(rs.getString("contatos.telefoneComercial"))));
                patrocinadorListar.add(patrocinador);

            }
            conect.fecharConexao();
        } catch (Exception e) {
            System.err.println("Erro ao listar tabela patrocinadores. " + e);
        }
        return patrocinadorListar;

    }
    
    public ArrayList<Patrocinador> listarRazaoSocial() throws SQLException {
        ArrayList<Patrocinador> listarRazaoSocial = new ArrayList<Patrocinador>();
        try {
            Conexao conect = new Conexao();
             PreparedStatement st = conect.getConnection().prepareStatement("SELECT idPatrocinador, razaoSocial FROM patrocinador ");
             ResultSet rs = st.executeQuery();
             while (rs.next()){
                 Patrocinador patrocinador = new Patrocinador();
                 patrocinador.setRazaoSocial(rs.getString("patrocinador.razaoSocial"));
                 patrocinador.setIdPatrocinador(rs.getInt("patrocinador.idPatrocinador"));
                 listarRazaoSocial.add(patrocinador);
             }
             conect.fecharConexao();
        }catch (SQLException e) {
            System.err.println("Erro SQL ao salvar o patrocinador " + e);
        } catch (Exception erro) {
            System.err.println("Erro ao listar a razao social" + erro);
        }
        return listarRazaoSocial;
    }

    public static void atualizar(Patrocinador patrocinador) {
        if (patrocinador == null) {
            JOptionPane.showMessageDialog(null, "patrocinador nao encontrado...");
        }
        try {
            Conexao conect = new Conexao();
            String sql;
            sql = "UPDATE patrocinador INNER JOIN contatos ON patrocinador.idPatrocinador = contatos.idPatrocinador_fk "
                    + "INNER JOIN endereco ON patrocinador.idPatrocinador = endereco.idPatrocinador_fk SET "
                    + "patrocinador.razaoSocial = ?, patrocinador.cnpj = ?, patrocinador.inscricaoEstadual = ?, "
                    + "patrocinador.inscricaoMunicipal = ?, patrocinador.finalidadeLucrativa = ?, patrocinador.nomeFantasia =?, "
                    + "contatos.telefoneComercial =?, contatos.celular = ?, contatos.email = ?, contatos.webSite =?, "
                    + "endereco.rua = ?, endereco.numero =?, endereco.bairro =?, endereco.cidade = ?, "
                    + "endereco.estado = ? , endereco.pais =?, endereco.complemento =?, endereco.cep= ? "
                    + "WHERE patrocinador.idPatrocinador = ? ";
            PreparedStatement st = conect.getConnection().prepareStatement(sql);
            st.setString(1, patrocinador.getRazaoSocial());
            st.setString(2, patrocinador.getCnpj());
            st.setString(3, patrocinador.getInscricaoEstadual());
            st.setString(4, patrocinador.getInscricaoMunicipal());
            st.setString(5, patrocinador.getFinalidadeLucrativa());
            st.setString(6, patrocinador.getNomeFantasia());
            st.setString(7, patrocinador.getContato().getTelefoneComercial());
            st.setString(8, patrocinador.getContato().getCelular());
            st.setString(9, patrocinador.getContato().getEmail());
            st.setString(10, patrocinador.getContato().getWebSite());
            st.setString(11, patrocinador.getEndereco().getRua());
            st.setString(12, patrocinador.getEndereco().getNumero());
            st.setString(13, patrocinador.getEndereco().getBairro());
            st.setString(14, patrocinador.getEndereco().getCidade());
            st.setString(15, patrocinador.getEndereco().getEstado());
            st.setString(16, patrocinador.getEndereco().getPais());
            st.setString(17, patrocinador.getEndereco().getComplemento());
            st.setString(18, patrocinador.getEndereco().getCep());
            st.setInt(19, patrocinador.getIdPatrocinador());
            st.executeUpdate();
            JOptionPane.showMessageDialog(null, "Patrocinador Atualizado com sucesso.");
            conect.fecharConexao();
        } catch (SQLException sql) {
            System.err.println("erro SQL " + sql);
        } catch (HeadlessException e) {
            System.err.println("erro ao atualizar a patrocinador no BD " + e);
        }

    }
}
