/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import controle.InformacoesMedicas;
import controle.Jogador;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import persistencia.Conexao;

/**
 *
 * @author laiendercamargos
 */
public class InformacoesMedicasDAO {
    
    public static boolean persistir(Jogador informacao) throws Exception{
        try{
            
            Conexao conect = new Conexao();
            Statement st = conect.getSt();
            java.sql.Date ultimaConsulta = new java.sql.Date(informacao.getInformacoa().getDataRevisao().getTime());
            java.sql.Date proximaConsulta = new java.sql.Date(informacao.getInformacoa().getProximaRevisao().getTime());
            JOptionPane.showMessageDialog(null, "idJogador.  " + informacao.getInformacoa().getIdJogador());
            st.execute("insert into informacoesMedicas (dataUltimaRevisao, dataProximaRevisao, restricaoMedicamento, "
                + " informacoesMedica, tipoSanguinio, idJogador_fk) values('"+ ultimaConsulta + "','"
                + proximaConsulta + "','"
                + informacao.getInformacoa().getRestricaoMedicamento() + "','"
                + informacao.getInformacoa().getComentarioMedico() +"','"
                + informacao.getInformacoa().getTipoSanguineo() + "','"
                + informacao.getInformacoa().getIdJogador()+
                "')");
            conect.fecharConexao();
            }catch(SQLException u){
            System.err.println("Erro no SQL. " + u);
            u.printStackTrace();
        }catch(Exception e){
            System.err.println("clase DAO erro ao salvar as infomações medicas.. " + e);
            e.printStackTrace();
        
        }
        finally{
           
        }
        
        
        return false;
        
    }
    
    public static Jogador pesquisar(Jogador jogador, int idJogador) throws Exception{
        //int idJogador = informacao.getIdJogador();
        JOptionPane.showMessageDialog(null, "idJogador pesquisa " + idJogador);
        
        try {
            Conexao conect = new Conexao();
            PreparedStatement st = conect.getConnection().prepareStatement("SELECT "
                    + "informacoesMedicas.dataUltimaRevisao,"
                    + "informacoesMedicas.dataProximaRevisao,"
                    + "informacoesMedicas.restricaoMedicamento,"
                    + "informacoesMedicas.informacoesMedica,"
                    + "informacoesMedicas.tipoSanguinio "
                    + "FROM informacoesMedicas "
                    + "WHERE idJogador_fk = ? ");
            
            st.setInt(1, idJogador);
            ResultSet rs = st.executeQuery();
            if(rs.next()){
                
                jogador.setInformacoa(new InformacoesMedicas(
                (rs.getDate("informacoesMedicas.dataUltimaRevisao")),
                (rs.getDate("informacoesMedicas.dataProximaRevisao")),
                (rs.getString("informacoesMedicas.restricaoMedicamento")),
                (rs.getString("informacoesMedicas.informacoesMedica")),
                (rs.getString("informacoesMedicas.tipoSanguinio"))));
                conect.fecharConexao();
            }
        } catch (SQLException e) { 
            System.err.println("Erro SQL na pesquisa de informacoes medicas na DAO.  " + e);
            e.printStackTrace();
        }catch(Exception erro){
            System.err.println("Erro na pesquisa de informacoes medicas. " + erro);
            erro.printStackTrace();
        }
        
        
        return jogador;
        
    }

    //public static Jogador pesquisa(Jogador jogador) {
        
    //    return jogador;
    //}

    public static void atualizar(Jogador jogadorAtualizar) {
        
        if(jogadorAtualizar == null){
            JOptionPane.showMessageDialog(null, "informacoes do jogador nao encontrado.");
        }
        
        try {
            Conexao conect = new Conexao();
            String sql;
        } catch (Exception e) {
        }
    }
}
    
    
