/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import controle.Conta;
import controle.Contato;
import controle.Despesa;
import controle.Endereco;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import persistencia.Conexao;

/**
 *
 * @author laiendercamargos
 */
public class CadastroFaturaDAO {

    public static boolean persistir(Despesa despesa) throws SQLException {
        
        try {
            Conexao conect = new Conexao();
            Statement st = conect.getSt();
            
            st.executeUpdate("insert into conta (banco, numeroConta, agencia, titularConta, codigoOrdenacao) values ('"
                    + despesa.getConta().getBanco() + "','"
                    + despesa.getConta().getNumeroConta() + "','"
                    + despesa.getConta().getNumeroAgencia() + "','"
                    + despesa.getConta().getTitularConta() + "','"
                    + despesa.getConta().getCodigoOrdenacao() 
                    + "')", Statement.RETURN_GENERATED_KEYS);
            final ResultSet rs= st.getGeneratedKeys();
            
            if( rs.next()){
                despesa.setConta(new Conta(rs.getInt(1)));
            }
            
            st.executeUpdate("insert into endereco (pais, estado, cidade, cep, complemento) values('" 
                    + despesa.getEndereco().getPais() + "','"
                    + despesa.getEndereco().getEstado() + "','"
                    + despesa.getEndereco().getCidade() + "','"
                    + despesa.getEndereco().getCep() + "','"
                    + despesa.getEndereco().getComplemento() 
                    + "')", Statement.RETURN_GENERATED_KEYS);
            final ResultSet rsEndereco = st.getGeneratedKeys();
            
            if(rsEndereco.next()){
                despesa.setEndereco(new Endereco(rsEndereco.getInt(1)));
            }
            
            st.executeUpdate("insert into contatos(email, telefoneComercial) values ('" 
                    + despesa.getContato().getEmail() + "','" 
                    + despesa.getContato().getTelefoneComercial() 
                    + "')", Statement.RETURN_GENERATED_KEYS);
            
            final ResultSet rsContatos = st.getGeneratedKeys();
            
            if(rsContatos.next()){
                despesa.setContato(new Contato(rsContatos.getInt(1)));
            }
            
            java.sql.Date dataVencimento = new java.sql.Date(despesa.getPrazoVencimento().getTime());
            
            st.executeUpdate("insert into cadastroDespesa(tipoCobranca, ivaPadrao, numeroRegistro, moeda, vencimento, valor,"
                    + " idConta_fk, idContatos_fk, idEndereco_fk ) values('"
                    + despesa.getTituloCobranca()+ "','"
                    + despesa.getIvaPadrao() + "','"
                    + despesa.getNumeroRegistro() +"','" 
                    + despesa.getMoeda() +"','" 
                    + dataVencimento +"','"
                    + despesa.getValor() + "','"
                    + despesa.getConta().getIdConta() + "','"
                    + despesa.getEndereco().getIdEndereco() + "','"
                    + despesa.getContato().getIdContato()
                    +"')");
            conect.fecharConexao();
        } catch (SQLException e) {
            System.err.println("Erro SQL " + e );
        } catch(Exception u){
            System.err.println("erro ao salvar." +u);
        }
        return false;
    }
    
    public ArrayList<Despesa> listar() throws SQLException{
        ArrayList<Despesa> listarDespesas = new ArrayList<Despesa>();
        try {
            Conexao conect = new Conexao();
            PreparedStatement st = conect.getConnection().prepareStatement("SELECT * FROM cadastroDespesa");
            ResultSet rs = st.executeQuery();
            while(rs.next()){
                Despesa despesa = new Despesa();
                despesa.setPrazoVencimento(rs.getDate("vencimento"));
                despesa.setValor(rs.getDouble("valor"));
                despesa.setTituloCobranca(rs.getString("tipoCobranca"));
                listarDespesas.add(despesa);
            }
            conect.fecharConexao();
        } catch (SQLException e) {
            System.err.println("erro sql " + e);
        }catch (Exception u){
            System.err.println("Erro ao listar despesas " + u);
        }
        return listarDespesas;
    }
    
    public ArrayList<Conta> listarContaBancaria() throws SQLException {
        ArrayList<Conta> listarRazaoSocial = new ArrayList<Conta>();
        try {
            Conexao conect = new Conexao();
             PreparedStatement st = conect.getConnection().prepareStatement("SELECT * FROM conta");
             ResultSet rs = st.executeQuery();
             while (rs.next()){
                 Conta conta = new Conta();
                 conta.setBanco(rs.getString("conta.banco"));
                 conta.setNumeroConta(rs.getString("conta.numeroConta"));
                 conta.setNumeroAgencia(rs.getString("conta.agencia"));
                 conta.setTitularConta(rs.getString("conta.titularConta"));
                 conta.setTipoConta(rs.getString("conta.tipoConta"));
                 
                 //patrocinador.setIdPatrocinador(rs.getInt("patrocinador.idPatrocinador"));
                 listarRazaoSocial.add(conta);
             }
             conect.fecharConexao();
        }catch (SQLException e) {
            System.err.println("Erro SQL ao salvar o patrocinador " + e);
        } catch (Exception erro) {
            System.err.println("Erro ao listar a razao social" + erro);
        }
        return listarRazaoSocial;
    }
    
}
