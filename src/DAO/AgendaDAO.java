/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import controle.Agenda;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;
import org.hsqldb.Library;
import persistencia.Conexao;

/**
 *
 * @author laiendercamargos
 */
public class AgendaDAO {
    
    public static boolean persistir(Agenda agenda){
        try {
            Conexao conect = new Conexao();
            Statement st = conect.getSt();
            java.sql.Date dataEvento = new java.sql.Date(agenda.getData().getTime());
            st.executeUpdate("insert into agenda(dataEvento, tituloEvento, localEvento, "
                    + "descricaoEvento, horaEvento, convidadoEvento) values ('" 
                    + dataEvento + "','"
                    + agenda.getTitulo() + "','"
                    + agenda.getLocal() + "','"
                    + agenda.getDescricao() + "','"
                    + agenda.getHora() + "','"
                    + agenda.getConvidados()
                    + "')");
            
        } catch (SQLException e) {
            System.err.println("Erro SQL ao salvar o evento DAO " + e);
            e.printStackTrace();
        }catch(Exception erro){
            System.err.println("Erro ao Salvar o evento DAO" + erro);
            erro.printStackTrace();
        }
        
        return false;
    }

    public static void atualizar(Agenda agenda) {
        if(agenda == null){
            JOptionPane.showMessageDialog(null, "dados do evento nao encontrado.");
        }
        try {
            Conexao conect = new Conexao();
            String sql;
             java.sql.Date dataEvento = new java.sql.Date(agenda.getData().getTime());
             sql = "UPDATE agenda SET agenda.dataEvento =?, agenda.tituloEvento =?, agenda.localEvento =?,"
                     + "agenda.descricaoEvento =?, agenda.horaEvento =?, agenda.convidadoEvento =?"
                     + "WHERE agenda.idAgenda = ?";
             
             PreparedStatement st = conect.getConnection().prepareStatement(sql);
             st.setDate(1, dataEvento);
             st.setString(2, agenda.getTitulo());
             st.setString(3, agenda.getLocal());
             st.setString(4, agenda.getDescricao());
             st.setString(5, agenda.getHora());
             st.setString(6, agenda.getConvidados());
             st.setInt(7, agenda.getIdAgenda());
             st.executeUpdate();
             
             JOptionPane.showMessageDialog(null, "Evento Atualizado.");
             conect.fecharConexao();
            
        } catch (SQLException e) {
            System.err.println("Erro SQL ao atualizar a Agenda de eventos" + e);
            e.printStackTrace();
        }catch(Exception u ){
            System.err.println("Erro ao atualizar a Agenda de eventos " + u);
            u.printStackTrace();
        }
        
    }

    public static void delete(int idAgenda) {
        try{
            Conexao conect = new Conexao();
            
            String sql = "DELETE FROM agenda WHERE idAgenda = ? ";
            PreparedStatement st = conect.getConnection().prepareStatement(sql);
            st.setInt(1, idAgenda);
            st.executeUpdate();
            conect.fecharConexao();
        }catch(SQLException erro){
            System.err.println("Erro SQL ao deletar Evento " + erro);
            erro.printStackTrace();
        }catch(Exception e){
            System.err.println("Erro ao deletar evento " + e);
            e.printStackTrace();
        }
    }
    
    public ArrayList<Agenda> listar() throws SQLException{
        ArrayList<Agenda> agenda = new ArrayList<Agenda>();
        
        try {
            Conexao conect = new Conexao();
            PreparedStatement st = conect.getConnection().prepareStatement("SELECT * FROM agenda ");
                    //+ "WHERE tituloEvento ORDER BY dataEvento");
            
            ResultSet rs = st.executeQuery();
            
            while(rs.next()){
                Agenda  agendaBusca = new Agenda();
                agendaBusca.setIdAgenda(rs.getInt("agenda.idAgenda"));
                agendaBusca.setTitulo(rs.getString("agenda.tituloEvento"));
                agendaBusca.setData(rs.getDate("agenda.dataEvento"));
                agendaBusca.setLocal(rs.getString("agenda.localEvento"));
                agendaBusca.setHora(rs.getString("agenda.horaEvento"));
                agendaBusca.setDescricao(rs.getString("agenda.descricaoEvento"));
                agendaBusca.setConvidados(rs.getString("agenda.convidadoEvento"));
                
                agenda.add(agendaBusca);
            }
            conect.fecharConexao();
        } catch (SQLException erro) {
            System.err.println("Erro SQL ao listar os eventos "+ erro);
            erro.printStackTrace();
        } catch(Exception e){
            System.err.println("Erro ao listar os eventos "+ e);
            e.printStackTrace();
        }
        
        return agenda;
    }
    
    public ArrayList<Agenda> listarDia() throws SQLException{
        ArrayList<Agenda> agenda = new ArrayList<Agenda>();
        
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

            //String format;
            sdf.format( new Date() );
            Conexao conect = new Conexao();
            PreparedStatement st = conect.getConnection().prepareStatement("SELECT * FROM agenda "
                    + "WHERE dataEvento = CURDATE() ORDER BY horaEvento");
            
            ResultSet rs = st.executeQuery();
            
            
            while(rs.next()){
                Agenda  agendaBusca = new Agenda();
                agendaBusca.setTitulo(rs.getString("agenda.tituloEvento"));
                agendaBusca.setData(rs.getDate("agenda.dataEvento"));
                agendaBusca.setLocal(rs.getString("agenda.localEvento"));
                agendaBusca.setHora(rs.getString("agenda.horaEvento"));
                
                agenda.add(agendaBusca);
            }
            conect.fecharConexao();
        } catch (SQLException erro) {
            System.err.println("Erro SQL ao listar os eventos "+ erro);
            erro.printStackTrace();
        } catch(Exception e){
            System.err.println("Erro ao listar os eventos "+ e);
            e.printStackTrace();
        }
        
        return agenda;
    }
}
