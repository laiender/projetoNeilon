/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visao;

import Controller.CadastroJogadorController;
import DAO.AgendaDAO;
import DAO.JogadorDAO;
import DAO.StaffDAO;
import controle.Agenda;
import controle.Jogador;
import controle.Pessoa;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import net.sf.jasperreports.engine.JRResultSetDataSource;

/**
 *
 * @author laiender.morais
 */
public class TelaPrincipal extends javax.swing.JFrame {

    /**
     * Creates new form telaPrincipal
     */
    public TelaPrincipal() {
        initComponents();
        setResizable(false);
        setLocationRelativeTo(null);
        pegarResolucao();
        //Agenda agenda = new Agenda();
         DefaultTableModel modelo = (DefaultTableModel) AgendaDia.getModel();
         AgendaDia.setRowSorter(new TableRowSorter(modelo));
         
         listarAgendaDia();
         listarAniversarioJogador();
         listaAniversarioStaff();
    }
    
    public void pegarResolucao(){
        Toolkit t = Toolkit.getDefaultToolkit();
        Dimension dimensao = t.getScreenSize();
        this.setSize((dimensao.width +5), (dimensao.height -38));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator1 = new javax.swing.JSeparator();
        jMenu5 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableAniversarioJogador = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        tabelaAniversarioStaff = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        AgendaDia = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        menuJogador = new javax.swing.JMenuItem();
        menuSocioTorcedor = new javax.swing.JMenuItem();
        menuPatrocinador = new javax.swing.JMenuItem();
        menuStaff = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuOrcamento = new javax.swing.JMenu();
        jMenuFaturas = new javax.swing.JMenuItem();
        jMenu6 = new javax.swing.JMenu();
        jogadores = new javax.swing.JMenuItem();

        jMenu5.setText("jMenu5");

        jMenuItem2.setText("jMenuItem2");

        jMenuItem1.setText("jMenuItem1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Tela Inicial");

        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel2.setPreferredSize(new java.awt.Dimension(120, 120));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 116, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 116, Short.MAX_VALUE)
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        tableAniversarioJogador.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome:", "Data Nascimento:", "Posição:"
            }
        ));
        jScrollPane1.setViewportView(tableAniversarioJogador);

        tabelaAniversarioStaff.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome:", "Data de Nascimento:", "Função"
            }
        ));
        jScrollPane3.setViewportView(tabelaAniversarioStaff);

        jLabel1.setText("Aniversario dos Jogadores");

        jLabel2.setText("Aniversario dos Staff");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 672, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(233, 233, 233)
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(252, 252, 252)
                .addComponent(jLabel2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addGap(15, 15, 15)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        AgendaDia.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Titulo:", "Data:", "Local:", "Hora:"
            }
        ));
        jScrollPane2.setViewportView(AgendaDia);

        jLabel3.setText("Agenda");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 638, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addGap(295, 295, 295))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 505, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jMenu1.setText("Arquivos");
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Cadastro");
        jMenu2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu2ActionPerformed(evt);
            }
        });

        menuJogador.setText("Jogador");
        menuJogador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuJogadorActionPerformed(evt);
            }
        });
        jMenu2.add(menuJogador);

        menuSocioTorcedor.setText("Socio Torcedor");
        menuSocioTorcedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSocioTorcedorActionPerformed(evt);
            }
        });
        jMenu2.add(menuSocioTorcedor);

        menuPatrocinador.setText("Cadastro de Patrocinador");
        menuPatrocinador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuPatrocinadorActionPerformed(evt);
            }
        });
        jMenu2.add(menuPatrocinador);

        menuStaff.setText("STAFF");
        menuStaff.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuStaffActionPerformed(evt);
            }
        });
        jMenu2.add(menuStaff);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Calendario");

        jMenuItem3.setText("Agenda");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem3);

        jMenuBar1.add(jMenu3);

        jMenuOrcamento.setText("Fatura");

        jMenuFaturas.setText("Orcamento");
        jMenuFaturas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuFaturasActionPerformed(evt);
            }
        });
        jMenuOrcamento.add(jMenuFaturas);

        jMenuBar1.add(jMenuOrcamento);

        jMenu6.setText("Relatorio");

        jogadores.setText("Jogadores");
        jogadores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jogadoresActionPerformed(evt);
            }
        });
        jMenu6.add(jogadores);

        jMenuBar1.add(jMenu6);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        setSize(new java.awt.Dimension(1370, 730));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void menuJogadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuJogadorActionPerformed
         
        try {
            TelaJogador jogador = new TelaJogador();
            jogador.setVisible(true);
            dispose();
        } catch (SQLException ex) {
            Logger.getLogger(TelaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
        
// TODO add your handling code here:
    }//GEN-LAST:event_menuJogadorActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
    Calendario calendario;
        try {
            calendario = new  Calendario();
            calendario.setVisible(true);
            dispose();
        } catch (SQLException ex) {
            Logger.getLogger(TelaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
    
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenu2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu2ActionPerformed

    // TODO add your handling code here:
    }//GEN-LAST:event_jMenu2ActionPerformed

    private void menuSocioTorcedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSocioTorcedorActionPerformed
         
        try {
            TelaSocioTorcedor torcedor = new TelaSocioTorcedor();
            torcedor.setVisible(true);
            dispose();
        } catch (SQLException ex) {
            Logger.getLogger(TelaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // TODO add your handling code here:
    }//GEN-LAST:event_menuSocioTorcedorActionPerformed

    private void jMenuFaturasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuFaturasActionPerformed
       
        try {
            Orcamento orcamento = new Orcamento();
            orcamento.setVisible(true);
            dispose();
        } catch (SQLException ex) {
            Logger.getLogger(TelaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuFaturasActionPerformed

    private void menuPatrocinadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuPatrocinadorActionPerformed
        try {
            TelaPatrocinador patrocinador = new TelaPatrocinador();
            patrocinador.setVisible(true);
            dispose();
            // TODO add your handling code here:
        } catch (SQLException ex) {
            System.err.println("Erro ao inicializar a tela Patrocinador: " + ex);
        }
    }//GEN-LAST:event_menuPatrocinadorActionPerformed

    private void jogadoresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jogadoresActionPerformed
        try {
            CadastroJogadorController.relatorio();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "erro ao selecionar o menu de relatorio "+e);
        }
        
        
    }//GEN-LAST:event_jogadoresActionPerformed

    private void menuStaffActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuStaffActionPerformed
        // TODO add your handling code here:
        try{
        TelaStaff staff = new TelaStaff();
        staff.setVisible(true);
        dispose();
        }catch(Exception e){
            System.err.println("Erro ao inicialiar a tela de Staff: " + e);
        }
    }//GEN-LAST:event_menuStaffActionPerformed

    public void listarAgendaDia() {
        
         DefaultTableModel modelo = (DefaultTableModel) AgendaDia.getModel();
         AgendaDAO agendaDAO = new AgendaDAO();
         
        try {
            
            for(Agenda m: agendaDAO.listarDia()){
                modelo.addRow(new Object[]{
                    m.getTitulo(),
                    m.getData(),
                    m.getLocal(),
                    m.getHora()
                });
                 
            }
           
            
            //agenda.getRowSorter().toggleSortOrder(3);
        } catch (SQLException ex) {
            System.err.println("Erro SQL na tabela do dia. " + ex);
            ex.printStackTrace();
        }catch(Exception e){
            System.err.println("erro na tabela do dia " + e);
            e.printStackTrace();
        }
        
    }
    
    public void listarAniversarioJogador(){
        DefaultTableModel modeloAniversario = (DefaultTableModel) tableAniversarioJogador.getModel();
        JogadorDAO jogadorDAO = new JogadorDAO();
        
        try {
            for(Jogador m: jogadorDAO.listar()){
                modeloAniversario.addRow(new Object[]{
                    m.getNome(),
                    m.getDataNascimento(),
                    m.getPosicao()
                });
            }
        }catch(Exception e){
            System.err.println("erro na tabela de aniversario." + e);
            e.printStackTrace();
        }
    }
    
    public void listaAniversarioStaff(){
        DefaultTableModel modeloAniversarioStaff = (DefaultTableModel) tabelaAniversarioStaff.getModel();
        StaffDAO staffDAO = new StaffDAO();
        
        try {
            for(Pessoa m: staffDAO.listar()){
                modeloAniversarioStaff.addRow(new Object[]{
                    m.getNome(),
                    m.getDataNascimento(),
                    m.getFuncao()
                });
                
            }
        } catch (Exception e) {
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new TelaPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable AgendaDia;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuFaturas;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenu jMenuOrcamento;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JMenuItem jogadores;
    private javax.swing.JMenuItem menuJogador;
    private javax.swing.JMenuItem menuPatrocinador;
    private javax.swing.JMenuItem menuSocioTorcedor;
    private javax.swing.JMenuItem menuStaff;
    private javax.swing.JTable tabelaAniversarioStaff;
    private javax.swing.JTable tableAniversarioJogador;
    // End of variables declaration//GEN-END:variables

    private Calendario Calendario() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
