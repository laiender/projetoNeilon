/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import java.util.Date;

/**
 *
 * @author joelson
 */
public class Despesa {
    private String tituloCobranca;
    private String ivaPadrao;
    private String moeda;
    private Date prazoVencimento;
    private String titularDespesa;
    private Conta conta;
    private Contato contato;
    private Endereco endereco;
    private String numeroRegistro;
    private double valor;

    public Despesa() {
    }
    
    public Despesa(String tituloCobranca, String ivaPadrao, String moeda, Date prazoVencimento, String titularDespesa,
            Conta conta, Contato contato, Endereco endereco, String numeroRegistro, double valor) {
        this.tituloCobranca = tituloCobranca;
        this.ivaPadrao = ivaPadrao;
        this.moeda = moeda;
        this.prazoVencimento = prazoVencimento;
        this.titularDespesa = titularDespesa;
        this.conta = conta;
        this.contato = contato;
        this.endereco = endereco;
        this.numeroRegistro= numeroRegistro;
        this.valor = valor;
    }

    public String getTituloCobranca() {
        return tituloCobranca;
    }

    public void setTituloCobranca(String tituloCobranca) {
        this.tituloCobranca = tituloCobranca;
    }

    public String getIvaPadrao() {
        return ivaPadrao;
    }

    public void setIvaPadrao(String ivaPadrao) {
        this.ivaPadrao = ivaPadrao;
    }

    public String getMoeda() {
        return moeda;
    }

    public void setMoeda(String moeda) {
        this.moeda = moeda;
    }

    public Date getPrazoVencimento() {
        return prazoVencimento;
    }

    public void setPrazoVencimento(Date prazoVencimento) {
        this.prazoVencimento = prazoVencimento;
    }

    public String getTitularDespesa() {
        return titularDespesa;
    }

    public void setTitularDespesa(String titularDespesa) {
        this.titularDespesa = titularDespesa;
    }

    public Conta getConta() {
        return conta;
    }

    public void setConta(Conta conta) {
        this.conta = conta;
    }

    public Contato getContato() {
        return contato;
    }

    public void setContato(Contato contato) {
        this.contato = contato;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public String getNumeroRegistro() {
        return numeroRegistro;
    }

    public void setNumeroRegistro(String numeroRegistro) {
        this.numeroRegistro = numeroRegistro;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    @Override
    public String toString() {
        return "Despesa{" + "tituloCobranca= " + tituloCobranca + ", ivaPadrao= " +
                ivaPadrao + ", moeda= " + moeda + ", prazoVencimento= " + prazoVencimento + 
                ", titularDespesa= " + titularDespesa + ", conta= " + conta + ", contato= " 
                + contato + ", endereco= " + endereco + ", numeroRegistro= " + numeroRegistro +
                ", valor= " + valor + '}';
    }

   
   

    
}
