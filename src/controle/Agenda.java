/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import java.util.Date;

/**
 *
 * @author laiendercamargos
 */
public class Agenda {
    private int idAgenda;
    private Date data;
    private String titulo;
    private String local;
    private String descricao;
    private String hora;
    private String convidados;

    public Agenda() {
    }
    
    

    public Agenda(Date data, String titulo, String local, String descricao, String hora, String convidados) {
        this.data = data;
        this.titulo = titulo;
        this.local = local;
        this.descricao = descricao;
        this.hora = hora;
        this.convidados = convidados;
    }

    public int getIdAgenda() {
        return idAgenda;
    }

    public void setIdAgenda(int idAgenda) {
        this.idAgenda = idAgenda;
    }
    
    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getConvidados() {
        return convidados;
    }

    public void setConvidados(String convidados) {
        this.convidados = convidados;
    }

    @Override
    public String toString() {
        return "agenda{" + "data=" + data + ", titulo=" + titulo + 
                ", local=" + local + ", descricao=" + descricao + 
                ", hora=" + hora + ", convidados=" + convidados + '}';
    }
    
    
}
