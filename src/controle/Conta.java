/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

/**
 *
 * @author joelson
 */
public class Conta {
    // banco, numeroConta, numeroAgencia, titularConta, tipoConta, codigoOrdenacao (poupança/corrente)
    private int idConta;
    private String banco;
    private String numeroConta;
    private String numeroAgencia;
    private String titularConta;
    private String tipoConta; // (0=poupança, 1=corrente, 2=salario)
    private String codigoOrdenacao;

    public Conta() {
    }

    public Conta(int idConta) {
        this.idConta = idConta;
    }
    
    
    public Conta( String banco, String numeroConta, String numeroAgencia, String titularConta, String tipoConta, String codigoOrdenacao) {
        
        this.banco = banco;
        this.numeroConta = numeroConta;
        this.numeroAgencia = numeroAgencia;
        this.titularConta = titularConta;
        this.tipoConta = tipoConta;
        this.codigoOrdenacao = codigoOrdenacao;
    }
    
    

    public Conta(String banco, String numeroConta, String numeroAgencia, String titularConta, String codigoOrdenacao) {
        //this.idConta = idConta;
        this.banco = banco;
        this.numeroConta = numeroConta;
        this.numeroAgencia = numeroAgencia;
        this.titularConta = titularConta;
        //this.tipoConta = tipoConta;
        this.codigoOrdenacao = codigoOrdenacao;
    }

    public int getIdConta() {
        return idConta;
    }

    public void setIdConta(int idConta) {
        this.idConta = idConta;
    }

    
    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getNumeroConta() {
        return numeroConta;
    }

    public void setNumeroConta(String numeroConta) {
        this.numeroConta = numeroConta;
    }

    public String getNumeroAgencia() {
        return numeroAgencia;
    }

    public void setNumeroAgencia(String numeroAgencia) {
        this.numeroAgencia = numeroAgencia;
    }

    public String getTitularConta() {
        return titularConta;
    }

    public void setTitularConta(String titularConta) {
        this.titularConta = titularConta;
    }

    public String getTipoConta() {
        return tipoConta;
    }

    public void setTipoConta(String tipoConta) {
        this.tipoConta = tipoConta;
    }

    public String getCodigoOrdenacao() {
        return codigoOrdenacao;
    }

    public void setCodigoOrdenacao(String codigoOrdenacao) {
        this.codigoOrdenacao = codigoOrdenacao;
    }

    @Override
    public String toString() {
        return "Conta{" + "banco= " + banco + ", numeroConta= " + numeroConta + ", numeroAgencia= "
                + numeroAgencia + ", titularConta= " + titularConta + ", tipoConta= " + tipoConta +
                ", codigoOrdenacao= " + codigoOrdenacao + '}';
    }
    
    
    
    
    
}
