/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import java.util.Date;

/**
 *
 * @author joelson
 */
public class Contrato {
    
    private Date inicio;
    private Date fim;
    private String clubeOrigem;
    private String empresario;
    private String telefoneEmpresario;
    private String clausulas;
    private double salario;
    
    
    // Classe empresário?
    

    public Contrato() {
    }

    public Contrato(Date inicio, String clausulas, double salario) {
        this.inicio = inicio;
        this.clausulas = clausulas;
        this.salario = salario;
    }
    
    

    public Contrato(Date inicio, Date fim, String clubeOrigem, 
            String empresario, String telefoneEmpresario, 
            String clausulas, double salario) {
        this.inicio = inicio;
        this.fim = fim;
        this.clubeOrigem = clubeOrigem;
        this.empresario = empresario;
        this.telefoneEmpresario= telefoneEmpresario;
        this.clausulas = clausulas;
        this.salario = salario;
    }

    public Date getInicio() {
        return inicio;
    }

    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    public Date getFim() {
        return fim;
    }

    public void setFim(Date fim) {
        this.fim = fim;
    }

    public String getClubeOrigem() {
        return clubeOrigem;
    }

    public void setClubeOrigem(String clubeOrigem) {
        this.clubeOrigem = clubeOrigem;
    }

    public String getEmpresario() {
        return empresario;
    }

    public void setEmpresario(String empresario) {
        this.empresario = empresario;
    }

    public String getTelefoneEmpresario() {
        return telefoneEmpresario;
    }

    public void setTelefoneEmpresario(String telefoneEmpresario) {
        this.telefoneEmpresario = telefoneEmpresario;
    }

    public String getClausulas() {
        return clausulas;
    }

    public void setClausulas(String clausulas) {
        this.clausulas = clausulas;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }
    
    @Override
    public String toString() {
        return "Contrato{" + "inicio=" + inicio + ", fim=" + fim + ", clubeOrigem=" +
                clubeOrigem + ", empresario=" + empresario + ", telefoneEmpresario=" + 
                telefoneEmpresario + ", clausulas=" + clausulas + ", Salario= "+ salario + '}';
    }
    
    

   
    

   
    
}
